<!-- section heading content -->
<?php $heading = "Where is FOX Sports Midwest Live?" ?>

<!-- location -->
<div class="section light-grey-container" id="location">
  <div class="container">

<!-- heading -->
    <div class="row">
      <div class="col-md-12 section-heading">
        <h2><?php echo $heading; ?></h2>
      </div>
    </div>

    <div class="row">
      <div class="container white-container">
        <div class="col-sm-4">
          <div class="thumbnail">
            <img src="http://www.stlouisdigitalsymposium.com/img/directions_1.jpg" class="img-responsive">
            <div class="caption">
              <h3>Ballpark Village
              <br>Fox Sports Midwest Live!</h3>
              <p>601 Clark Ave #103<br>St. Louis, MO 63103</p>
              <small>Self-Parking: FREE 2-hour parking with validation is available from 6am-9pm. Parking validation ticket must be obtained before 9pm.</small>
            </div>
          </div>
        </div>
        <div class="col-sm-8">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3117.0918933104285!2d-90.19225599999996!3d38.62376699999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87d8b31b77647b0d%3A0xce09535033fc72d4!2sFOX+Sports+Midwest+Live*21!5e0!3m2!1sen!2sus!4v1412694975398" width="100%" frameborder="0" style="border:0" height="500px"></iframe>
        </div>
      </div>
    </div>
    
  </div>
</div>
