<!-- hero -->
<div class="section" id="hero">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 text-left">
        <img src="img/digsym-logo-white.svg" class="img-responsive hero-logo">
      </div>
      <div class="col-sm-6 text-right">
        <h1>Wednesday, <br>November 16th 2016
        <br>12pm-5:30pm</h1>
        <h4>Networking Happy Hour 5:30pm-6:30pm</h4>
        <h2>Ballpark Village St. Louis
        <br>Fox Sports Live</h2>
        <p>Get connected, enlightened and inspired with other digital creatives, marketers, agencies and clients.</p>
      </div>
    </div>
  </div>
</div>

<!-- get tickets -->
<div class="section get-tickets-bar" id="get-tickets">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2>Seating is limited to 500 and tickets are going fast</h2>
      </div>
      <div class="col-md-4 text-center">
        <a class="btn-get-tix btn btn-block btn-lg" href="<?php echo $event_link ?>" target="_blank">SOLD OUT</a>
      </div>
    </div>
  </div>
</div>
