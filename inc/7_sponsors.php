<!-- section heading content -->
<?php $heading = "Presented By:" ?>

<!-- sponsors -->
<div class="section" id="sponsors">
  <div class="container">

<!-- heading -->
    <div class="row">
      <div class="col-md-12 section-heading">
        <h2><?php echo $heading; ?></h2>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <a href="http://adclubstl.org/" target="_blank"><img class="sponsor-p center-block" src="img/logo_adcl.svg" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-6">
        <a href="http://hlkagency.com/" target="_blank"><img class="sponsor-p center-block" src="img/logo_hlk.svg" type="image/svg+xml"></a>
      </div>
    </div>
    <!-- session sponsors -->
    <div class="row">
      <div class="col-md-12">
        <h3>Session Sponsors:</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <a href="http://www.visualiq.com/" target="_blank"><img class="sponsor-ses center-block" src="img/logo_visual.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://evolvedigitallabs.com/" target="_blank"><img class="sponsor-ses center-block" src="img/logo_evolve.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://manifest.com/" target="_blank"><img class="sponsor-ses center-block" src="img/logo_manifest.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://manifest.com/" target="_blank"><img class="sponsor-ses center-block" src="img/logo_power.png" type="image/svg+xml"></a>
      </div>
    </div>
    <!-- event sponsors -->
    <div class="row">
      <div class="col-md-12">
        <h3>Event Sponsors:</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <a href="http://creativesoncall.com/" target="_blank"><img class="sponsor-e center-block" src="img/logo_coc.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://www.theswitch.us/" target="_blank"><img class="sponsor-e center-block" src="img/logo_switch.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://www.wearetg.com/" target="_blank"><img class="sponsor-e center-block" src="img/logo_timm.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://www.jbeidlepr.com/" target="_blank"><img class="sponsor-e center-block" src="img/logo_beidle.png" type="image/svg+xml"></a>
      </div>
    </div>
    <!-- gold sponsors -->
    <div class="row">
      <div class="col-md-12">
        <h3>Gold Sponsors:</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <a href="http://avatar-studios.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_avatar.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://www.alignedmedia.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_aligned.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://www.cannonballagency.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_cannon.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3">
        <a href="http://rukuspost.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_rukus.png" type="image/svg+xml"></a>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <a href="http://www.bigclubdigital.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_bcd.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-4">
        <a href="https://www.nestlepurinacareers.com/career-opportunities/professional/checkmark/" target="_blank"><img class="sponsor-g center-block" src="img/logo_ckmk.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-4">
        <a href="http://www.centro.net/" target="_blank"><img class="sponsor-g center-block" src="img/logo_centro.png" type="image/svg+xml"></a>
      </div>
    </div>
    <!-- silver sponsors -->
    <div class="row">
      <div class="col-md-12">
        <h3>Silver Sponsors:</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3 col-xs-6">
        <a href="http://savvycoders.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_savvy.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://halskistudio.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_halski.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://www.menageriemodels.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_menagerie.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://www.rodgerstownsend.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_rt.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://www.lindenwood.edu/" target="_blank"><img class="sponsor-s center-block" src="img/logo_lind.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://brutonstroube.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_bruton.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://www.handlpartners.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_hl.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://osbornbarr.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_ob.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-offset-3 col-sm-3 col-xs-6">
        <a href="http://osbornbarr.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_90.png" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-3 col-xs-6">
        <a href="http://osbornbarr.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_bend.png" type="image/svg+xml"></a>
      </div>
    </div>
    <!-- event partners -->
    <div class="row">
      <div class="col-md-12">
        <h3>Event Partners:</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        <a href="http://314digital.com/" target="_blank"><img class="sponsor-ep center-block" src="img/logo_314.png" type="image/svg+xml"></a>
      </div>
      <div class="col-xs-6">
        <a href="http://www.smcstl.com/" target="_blank"><img class="sponsor-ep center-block" src="img/logo_smc.png" type="image/svg+xml"></a>
      </div>
    </div>
  </div>
</div>
