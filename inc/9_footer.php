<!-- footer -->
<footer class="section" id="foot">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <a href="http://adclubstl.org/" target="_blank"><img class="adcl-footer" src="img/logo_adcl_footer.svg" type="image/svg+xml"></a>
      </div>
      <div class="col-sm-4 text-center">
        <p><small>Copyright &copy; <?php echo date( 'Y' ); ?> | Microsite designed by <a href="http://www.wearetg.com" target="_blank">Timmermann Group</a></small></p>
      </div>
      <div class="col-sm-4">
        <div class="row">
          <div class="col-md-12 hidden-lg hidden-md hidden-sm text-center">
            <a href="<?php echo $facebook; ?>" target="_blank"><i class="icon-facebook-rect"></i></a>
            <a href="<?php echo $linkedin; ?>" target="_blank"><i class="icon-linkedin-rect"></i></a>
            <a href="<?php echo $twitter; ?>" target="_blank"><i class="icon-twitter-bird"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 hidden-xs text-right">
            <a class="top" href="#hero">Top<i class="icon-up-circle"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script type="text/html" src="./js/jquery.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/scroll-shift.js"></script>

</body>
</html>
