<?php require_once('functions.php'); ?>
<!DOCTYPE html>
<html lang="en-US" itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns#" >

  <head>
    <meta prefix="og: http://ogp.me/ns#" property="og:image:url" content="http://www.stlouisdigitalsymposium.com/img/stldigsym-og.jpg" />
    <meta prefix="og: http://ogp.me/ns#" property="og:image" content="http://www.stlouisdigitalsymposium.com/img/stldigsym-og.jpg" />
    <meta prefix="og: http://ogp.me/ns#" property="og:image:type" content="image/jpg" />
    <meta prefix="og: http://ogp.me/ns#" property="og:url" content="http://www.stlouisdigitalsymposium.com/"/>
    <meta prefix="og: http://ogp.me/ns#" property="og:title" content="Ad Club St. Louis Digital Symposium 2016" />
    <meta prefix="og: http://ogp.me/ns#" property="og:description" content="#STLDIGSYM connects, enlightens and inspires digital marketers from the agency, corporate and education realms with four key speaking sessions covering this year’s theme of Singularity."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="#STLDIGSYM connects, enlightens and inspires digital marketers from the agency, corporate and education realms with four key speaking sessions covering this year’s theme of Singularity.">
    <meta name="keywords" content="St Louis, Missouri, Adclub, Digital, Symposium, November, Ballpark, Village, Fox, Sports, Tickets, Stldigsym, Industry, Advertising, Ad, Amazon, Razorfish, Cooper, Creative, Adclubstl, Adclub, HLK, Crowdsource, Timmermann, Timmermanngroup, TG, Switch, Liberate, Manifest, Toky, Rodgers, Lindenwood, COC, Osborn, Barr, AAAAs, SMCSTL, 314Digital, Design, Marketing, Creative"/>
    <meta charset="UTF-8" />
    <meta name="description" content="Get connected, enlightened and inspired with other digital creatives, marketers, agencies and clients. ">

    <title>2016 St. Louis Digital Symposium | Ad Club STL and HLK present</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css">

    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="google-site-verification" content="0l5zBH4cgNBI8xNIejbMDCbg72YZiR3T1ESN4X09wAk" />

  <!-- google analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-45222419-1', 'stlouisdigitalsymposium.com');
    ga('send', 'pageview');
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SocialEvent",
      "name": "2016 St. Louis Digital Symposium",
      "url" : "http://www.stlouisdigitalsymposium.com/",
      "sponsor": {
        "@type": "Organization",
        "name": "Ad Club STL",
        "url": "http://adclubstl.org/"
            },
      "startDate": "2016-11-16T12:00",
      "endDate": "2016-11-16T17:30",
      "image": "http://www.stlouisdigitalsymposium.com/img/stldigsym-og.jpg",
      "offers": [
        {
          "@type": "Offer",
          "url": "http://www.eventbrite.com/e/stldigsym-st-louis-digital-symposium-2016-tickets-27416631884",
          "validFrom": "2016-10-01T00:01",
          "validThrough": "2016-11-15T23:59",
          "price": "40.00",
          "priceCurrency": "USD"
        }
        ],
      "description": "#STLDIGSYM connects, enlightens and inspires digital marketers from the agency, corporate and education realms with four key speaking sessions covering this year’s theme of Singularity.",
      "location": {
        "@type": "Place",
        "name": "Ballpark Village St. Louis",
        "url": "http://www.stlballparkvillage.com/",
        "address": {
          "@type": "PostalAddress",
          "addressLocality": "St. Louis",
          "addressRegion": "MO",
          "streetAddress": "601 Clark Avenue",
          "postalCode": "63102"
        },
        "hasMap": "https://www.google.com/maps/place/Ballpark+Village+St.+Louis/@38.6238991,-90.1941064,17z/data=!4m13!1m7!3m6!1s0x87d8b31b7f36e7e7:0x568acabfa33245df!2s601+Clark+Ave,+St.+Louis,+MO+63102!3b1!8m2!3d38.6238991!4d-90.1919177!3m4!1s0x87d8b31b9be97ba1:0xe4fa859bce3a212e!8m2!3d38.6237692!4d-90.1923841"
        },
        "performer": [
            {
                "@type": "Person",
                "image": "http://www.stlouisdigitalsymposium.com/img/speaker_1.jpg",
                "name": "Tom Anderson"

            },
            {
                "@type": "Person",
                "image": "http://www.stlouisdigitalsymposium.com/img/speaker_2.jpg",
                "name": "Brian Sawyer"

            },
            {
                "@type": "Person",
                "image": "http://www.stlouisdigitalsymposium.com/img/speaker_3.jpg",
                "name": "Mindie Kaplan",
                "sameAs": "https://twitter.com/mindiekaplan"

            },
            {
                "@type": "Person",
                "image": "http://www.stlouisdigitalsymposium.com/img/speaker_4.jpg",
                "name": "Tomiwa Alabi",
                "sameAs": "https://twitter.com/theway4ward"
            }
        ]
    }
    </script>

  </head>
<body>
