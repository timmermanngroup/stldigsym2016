<!-- section heading content -->
<?php $heading = 'Singularity' ?>

<!-- about -->
<div class="section" id="about">
  <div class="container">

    <div class="row">
      <div class="col-md-8">
        <h2><?php echo $heading; ?></h2>
        <h3 class="callout">Confronting the ever-accelerating progress of technology as it approaches the limits of comprehension.</h3>
        <p>Once a concept of science fiction, now a reality, digital singularity comes from the exponential increase in technology, intelligence and capability. As marketers, we embrace options –
        but the dizzying array of choices and channels can lead us to “paralysis by analysis.” How can we make the most intelligent decisions to move forward? Join #STLDIGSYM as we ask industry
        experts in creative, technology, content, data &amp; analytics to help navigate the imminent arrival of singularity.
        </p>
      </div>
    </div>

  </div>
</div>

<div class="section" id="digsym-about">
    <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2>About <a href="https://twitter.com/search?q=%23stldigsym&src=typd" target="_blank">#STLDIGSYM</a></h2>
            <p class = "sub"><a href="https://twitter.com/search?q=%23stldigsym&src=typd" target="_blank">#STLDIGSYM</a> is presented by AdClubSTL and HLK along with organization partners 314 Digital
            and Social Media Club St. Louis. This year features esteemed speakers and moderators from Apple, Build-a-Bear, Adobe, LaunchCode, Rated-VR, Visual IQ and more. Make sure to stick around
            for the networking happy hour to follow immediately after the last session concludes.
            </p>
          </div>
        </div>
    </div>
</div>
