<!-- menu items -->
<?php
  $menu_items = array(
    'about',
    'speakers',
    'agenda',
    'location',
    'sponsors'
  );
?>

<div id="navbar-wrap">
  <!-- banner -->
  <div id="navbar-banner">
    This year's Dig Sym is coming! <br class="hidden-md hidden-lg" /> Save the date: November 15, 2017
  </div>

  <!-- navigation -->
  <div class="navbar navbar-default navbar-fixed-top navbar-banner" id="main-nav">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand nav-icon hidden-xs" href="#hero"><img alt="Ad Club StL" src="img/logo_adcl.svg"></a>
        <a class="navbar-brand mobile-nav-icon hidden-sm hidden-md hidden-lg hidden-xl" href="#hero"><img alt="Ad Club StL" src="img/logo_adcl_footer.svg"></a>
        <a id="btn-get-tix" class="pull-right btn hidden-sm hidden-md hidden-lg" href="<?php echo $event_link ?>" target="_blank">SOLD OUT</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-left navbar-nav nav-ul hidden-sm">
            <?php foreach ($menu_items as $menu_item): ?>
                <li class="pure-menu-item"><a href="#<?php echo ($menu_item); ?>" class="pure-menu-link"><?php echo $menu_item ?></a></li>
            <?php endforeach ?>
        </ul>
        <ul class="nav navbar-right navbar-nav">
          <li>
            <a href="<?php echo $facebook; ?>" target="_blank"><i class="icon-facebook-rect"></i></a>
          </li>
          <li>
            <a href="<?php echo $linkedin; ?>" target="_blank"><i class="icon-linkedin-rect"></i></a>
          </li>
          <li>
            <a href="<?php echo $twitter; ?>" target="_blank"><i class="icon-twitter-bird"></i></a>
          </li>
          <li>
            <a class="btn" id="btn-get-tix" href="<?php echo $event_link ?>" target="_blank">SOLD OUT</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
