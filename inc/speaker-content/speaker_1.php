<?php
$speaker_1 = new Speaker(
/* name */        'Tom Anderson',
/* title */       'Regional VP Attribution-Insights',
/* employer */    'Visual IQ',
/* linkedin */    '',
/* twitter */     'https://twitter.com/visualiq',
/* category */    'Session 1: Data & Analytics',
/* description */ 'Attribution - When Data Pushes Proof … Looking at the new ROI (Return on Intelligence)',
/* content */     'Tom is a 15 year Digital Industry veteran with hands-on leadership experience across the breadth of Digital Advertising including IPTV. He is currently a Regional Vice President at Visual IQ, the market leader in multi-channel measurement and optimization.  Tom believes that one of the biggest challenges facing CMO’s today is optimizing marketing channel attribution across the fragmented consumer market. He’s held positions at Microsoft Advertising, comScore and an ad view ability and contextual start-up acquired by comScore.  Tom also advises early-stage ad tech startups and mobile app startups on monetization and marketing. His passion is connecting  people, purpose and partnerships that create mutual benefit. Tom loves to spend time with his new daughter and has another baby on the way in February.',
/* moderator */   '<a href="https://twitter.com/halbrook" target="_blank">Michael Halbrook, Sr. Manager, (Adobe Consulting)</a>',
/* panelists */   '<a href="https://twitter.com/davet_kidd22" target="_blank">David Kidd, Marketing Director (Steady Rain)</a><br><a href="https://www.linkedin.com/in/morganwallace" target="_blank">Morgan Wallace, Business Solutions Manager (Evolve) </a><br /> <a href="https://www.linkedin.com/in/paul-troia-18bbb219" target="_blank">Paul Troia, Assoc. Director, Analytical Operations (Centro)</a>'
);
?>
