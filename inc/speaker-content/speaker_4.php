<?php
$speaker_2 = new Speaker(
/* name */        'Tomiwa Alabi',
/* title */       'Sr Human Interface Designer',
/* employer */    'Apple',
/* linkedin */    '',
/* twitter */     'https://twitter.com/theway4ward',
/* category */    'Session 4: Creative',
/* description */ 'Convergence with Divergence – Shaping ideas from diverse thinking',
/* content */     'Tomiwa Alabi is a Senior Human Interface Designer for iOS and OSX at Apple. He applies
his experience to create meaningful interaction on mobile and web devices.
Tomiwa was born and raised in Kaduna, Nigeria and has worked for design shops
and marketing agencies from St. Louis to New York to San Francisco. While at
Wieden + Kennedy, Tomiwa designed digital experiences for ESPN, Nike and Target.
His passion and focus is on quality design – to achieve experiences that are tasteful,
deliberate and intuitive – and make a true connection between medium and brand.
',
/* moderator */   '<a href="https://twitter.com/rodgerstownsend" target="_blank">Crystal Merritt, VP, Director of Account Planning (Rodgers-Townsend)</a>',
/* panelists */   '<a href="https://twitter.com/emorrissey" target="_blank">Ed Morissey - CCO (Integrity)</a><br><a href="https://twitter.com/mspako" target="_blank">Mike Spakowski CD/Partner (Atomic Dust)</a><br><a href="https://twitter.com/rodgerstownsend" target="_blank">Mike McCormick CCO (Rodgers-Townsend)</a><br /> <a href="https://twitter.com/theslynch" target="_blank">Stephanie Lynch, Dir. Brand Market Strategy (Build-A-Bear)</a>'
);
?>
