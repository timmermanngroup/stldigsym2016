<?php
$speaker_2 = new Speaker(
/* name */        'Mindie Kaplan',
/* title */       'CEO',
/* employer */    'Rated VR',
/* linkedin */    '',
/* twitter */     'https://twitter.com/mindiekaplan',
/* category */    'Session 3: Technology',
/* description */ 'The Intersection of Technology & Future Scoping – Imagining possibilities and turning them into realities',
/* content */     'Mindie is the CEO/Founder of Rated VR, a strategy lab for brands and agencies. She has over 15 years of experience in digital media, tech and branding for Microsoft, Nissan and several others. Mindie is also a content creator who got her start as the creator and host of the Chicago Sun Times web series, “Happy Hour with Mindie” where she interviewed big names in the start up world, sports, music and culture. Her newest series features modern makers such as the “Fat Jew” in unique environments shot in VR. She continues to create new possibilities in the space for clients and viewers alike by seeing technology through a media lens. Upcoming initiatives include ongoing branded content, experiential activations and B2B innovation.',
/* moderator */   '<a href="https://twitter.com/Launch_Code" target="_blank">Mark Bauer, Executive Director (Launchcode)</a>',
/* panelists */   '<a href="https://twitter.com/emqueathem" target="_blank">Elaine Queathem, CEO (Savvy Coders)</a><br><a href="https://twitter.com/eTab_inc" target="_blank"> Jeff Stein, CEO (eTab)</a><br><a href="https://twitter.com/PeterDycus" target="_blank">Peter Dycus, Director of Development (HLK)</a>'
);
?>
