<?php
$speaker_2 = new Speaker(
/* name */        'Brian Sawyer',
/* title */       'Sr. Managing Director of Digital',
/* employer */    'Build-A-Bear',
/* linkedin */    'https://www.linkedin.com/in/chickfoxgrover',
/* twitter */     'https://twitter.com/buildabear',
/* category */    'Session 2: Content & Social',
/* description */ 'Creating Content To Increase Brand Power - How Digital Engagement Can Push Success',
/* content */     'Brian Sawyer has over 17 years of experience with multiple brands in ecommerce and interactive business. As Senior Managing Director: Digital for Build-A-Bear, he oversees digital marketing, ecommerce, interactive, guest services, and digital entertainment. Over the past year, Build-A-Bear has received acclaim for its Play Beyond Plush strategy which creates a seamless transition between the physical and digital engagement channels creating over 20M interactions for the brand.  Prior to joining Build-A-Bear Workshop in 2012, he served as Director of Ecommerce for Charming Charlie.',
/* moderator */   '<a href="https://twitter.com/ashlynbrewer" target="_blank">Ashlyn Brewer - SMCSTL Board President & Strategist at Standing Partnership</a>',
/* panelists */   '<a href="https://twitter.com/Brontchie" target="_blank">Bronwyn Ritchie - Content Strategist (Timmerman Group)</a><br><a href="https://twitter.com/Cardinals" target="_blank">Katie Brandenberg - Manager of Fan Entertainment (STL Cardinals)</a><br><a href="https://twitter.com/melissa_bouma" target="_blank">Melissa Bouma - VP, Acquisition & Analytics (Manifest) 
</a>'
);
?>
