<!-- section heading content -->
<?php $heading = "Gain a Fresh Perspective from these Industry Leaders"; ?>

<?php include 'speaker-content/speaker_1.php'; ?>
<?php include 'speaker-content/speaker_2.php'; ?>
<?php include 'speaker-content/speaker_3.php'; ?>
<?php include 'speaker-content/speaker_4.php'; ?>


<!-- Speakers Container-->
<div class="section speakers" id="speakers">
  <div class="container">

<!-- heading -->
    <div class="row">
      <div class="col-md-12 section-heading">
        <h2><?php echo $heading; ?></h2>
      </div>
    </div>

    <div class="row hidden-xs hidden-sm">

      <!-- speaker-buttons -->
      <div class="col-md-4" id="speakers-left">
        <ul class="media-list" role="tablist">
          <?php $k=1; ?>
          <?php foreach (Speaker::$allSpeakers as $speak): ?>
          <li class="media" data-target="#speaker_<?php echo $k; ?>" role="tab" data-toggle="tab" aria-controls="#speaker_<?php echo $k; ?>">
            <img class="media-object pull-left" src="img/speaker_<?php echo $k; ?>.jpg">
            <div class="media-body">
              <h4 class="media-heading"><?php echo ($speak->name); ?></h4>
              <p><?php echo ($speak->title); ?><br><small><?php echo ($speak->employer); ?></small></p>
            </div>
          </li>
          <?php $k++; ?>
          <?php endforeach; ?>
        </ul>
      </div>

      <!-- speaker-content -->
      <div class="col-md-8 speakers-white-box tab-content">
        <?php $i=1; ?>
        <?php foreach (Speaker::$allSpeakers as $speak): ?>
        <div role="tabpanel" class="tab-pane fade in <?php if ($i==1): echo ('active'); endif;?>" id="speaker_<?php echo $i; ?>">
          <?php include 'speaker-content/speaker_' . $i . '.php'; ?>
          <h4><?php echo ($speak->category); ?></h4>
          <h5><?php echo ($speak->description); ?></h5>
          <p><?php echo ($speak->content); ?></p>
          <div class="panels">
            <h5>Moderator:</h5>
            <p><?php echo ($speak->moderator); ?></p>
          </div>
          <div class="panels">
            <h5>Panelists:</h5>
            <p><?php echo ($speak->panelists); ?></p>
          </div>
          <div class="speaker-info">
            <p class="small">Session sponsored by:</p>
            <a href="<?php echo ($speak->sponsor); ?>" target="_blank"><img class="logo-sponsor" src="/img/logo_sponsor-<?php echo $i; ?>.png" alt="" /></a>
            <a href="<?php echo ($speak->twitter); ?>" class="social-icon" target="_blank"><i class="icon-twitter-bird"></i></a>
          </div>
        </div>
        <?php $i++; ?>
        <?php endforeach; ?>
      </div>

    </div>

<!-- Speakers Accordion (Mobile) -->
    <div class="row hidden-md hidden-lg hidden-xl">
      <div id="accordion" class="col-xs-12 panel-group">
        <div class="panel">
        <?php $a=1; ?>
        <?php foreach (Speaker::$allSpeakers as $speak): ?>

          <!-- Name -->
          <div class="acc-top row" id="heading<?php echo $a ?>" data-parent="#accordion" data-toggle="collapse" href="#collapse<?php echo $a ?>">
              <div class="col-xs-4 col-sm-3 acc-left">
                <img class="img-responsive" src="img/speaker_<?php echo $a ?>.jpg">
              </div>
              <div class="col-xs-8 col-sm-9 acc-right">
                <h4 class="media-heading"><?php echo ($speak->name); ?></h4>
                <p><?php echo ($speak->title); ?><br><small><?php echo ($speak->employer); ?></small></p>
              </div>
          </div>

          <!-- Info -->
          <div id="collapse<?php echo $a ?>" class="acc-white-box panel-collapse collapse <?php if ($a==1): echo ('in'); endif;?>">
            <?php include 'speaker-content/speaker_' . $a . '.php'; ?>
            <h4><?php echo ($speak->category); ?></h4>
            <h5><?php echo ($speak->description); ?></h5>
            <p><?php echo ($speak->content); ?></p>
            <div class="panels">
              <h5>Moderator:</h5>
              <p><?php echo ($speak->moderator); ?></p>
            </div>
            <div class="panels">
              <h5>Panelists:</h5>
              <p><?php echo ($speak->panelists); ?></p>
            </div>
            <div class="speaker-info">
              <p class="small">Session sponsored by:</p>
              <?php if ($a!=2): ?><a href="<?php echo ($speak->sponsor); ?>" target="_blank"><img class="logo-sponsor" src="/img/logo_sponsor-<?php echo $a; ?>.png" alt="" /></a><?php endif; ?>
              <a href="<?php echo ($speak->twitter); ?>" class="social-icon" target="_blank"><i class="icon-twitter-bird"></i></a>
            </div>
          </div>

        <?php $a++; if ($a>4):break;endif;?>
        <?php endforeach; ?>
        </div>
      </div>
    </div>

<!-- End Speakers Container -->
  </div>
</div>
