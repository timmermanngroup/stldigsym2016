<!-- section heading content -->
<?php $heading = "What's on Tap" ?>

<!-- events -->
<?php
$event_1 = new Event(
  '11:30a',
  'Doors and registration open in the Ballpark Village / Fox Sports Midwest Live lobby
  Each attendee receives one drink ticket and complimentary appetizer buffet access.  Cash bar, lunch, beer and beverage service available throughout <a href="https://twitter.com/search?q=%23STLDIGSYM&src=typd&lang=en" target="_blank">#STLDIGSYM</a>'
);

$event_2 = new Event(
  '12:30p',
  'Symposium start, complimentary appetizer buffet provided by Ad Club STL opens.</p>'
);

$event_3 = new Event(
  '12:35p',
  '<a href="https://twitter.com/visualiq" target="_blank">Tom Anderson, Regional VP Attribution/Insights - Visual IQ</a> leads the data/analytics session with his presentation on: Attribution - When Data Pushes Proof with moderator <a href="https://twitter.com/halbrook" target="_blank">Michael Halbrook, Sr. Manager (Adobe Consulting)</a> and panelists:  <a href="https://twitter.com/davet_kidd22" target="_blank">David Kidd, Marketing Director (Steady Rain)</a>, <a href="https://www.linkedin.com/in/morganwallace" target="_blank">Morgan Wallace, Business Solutions Manager (Evolve) </a>, <a href="https://www.linkedin.com/in/paul-troia-18bbb219" target="_blank">Paul Troia, Assoc. Director, Analytical Operations (Centro)</a>'
);

$event_4 = new Event(
  '1:40p',
  '<a href="https://twitter.com/buildabear" target="_blank">Brian Sawyer, Sr. Managing Director of Digital - Build a Bear</a> leads the content/social media session with his presentation on: Creating Content To Increase Brand Power - How Digital Engagement Can Push Success with moderator: <a href="https://twitter.com/ashlynbrewer" target="_blank">Ashlyn Brewer - SMCSTL Board President & Strategist at Standing Partnership</a> and panelists: <a href="https://twitter.com/Brontchie" target="_blank">Bronwyn Ritchie - Content Strategist (Timmerman Group)</a>, <a href="https://twitter.com/Cardinals" target="_blank">Katie Brandenberg - Manager of Fan Entertainment (STL Cardinals)</a>, <a href="https://twitter.com/melissa_bouma" target="_blank">Melissa Bouma, VP Acquisition & Analytics (Manifest)</a>'
);

$event_5 = new Event(
  '2:50p',
  'Break for beverages, snacks and networking'
);

$event_6 = new Event(
  '3:20p',
  '<a href="https://twitter.com/mindiekaplan" target="_blank">Mindie Kaplan, CEO - Rated VR</a> leads the “technology” session with her presentation: The Intersection of Technology & Future Scoping – Imagining possibilities and turning them into realities with moderator: <a href="https://twitter.com/Launch_Code" target="_blank">Mark Bauer, Executive Director (Launchcode)</a> and panelists: <a href="https://twitter.com/emqueathem" target="_blank">Elaine Queathem, CEO (Savvy Coders)</a>, <a href="https://twitter.com/eTab_inc" target="_blank">Jeff Stein, CEO (eTab)</a>, <a href="https://twitter.com/PeterDycus" target="_blank">Peter Dycus, Director of Development (HLK)</a>'
);

$event_7 = new Event(
  '4:25p',
  '<a href="" target="_blank">Tomiwa Alabi, Sr Human Interface Designer</a> - Apple leads the “creative” themed session with his presentation on:  Convergence with Divergence – Shaping ideas from diverse thinking with moderator: <a href="https://twitter.com/rodgerstownsend" target="_blank">Crystal Merritt - VP, Director of Account Planning, (Rodgers-Townsend)</a> and panelists: <a href="https://twitter.com/emorrissey" target="_blank">Ed Morrissey - CCO (Integrity)</a>, <a href="https://twitter.com/mspako" target="_blank">Mike Spakowski CD/Partner (Atomic Dust)</a>, <a href="https://twitter.com/rodgerstownsend" target="_blank">Mike McCormick CCO (R/T)</a>, <a href="https://twitter.com/theslynch" target="_blank">Stephanie Lynch, Dir. Brand Market Strategy (Build-A-Bear)</a>'
);

$event_8 = new Event(
  '5:30p',
  'Conference ends.'
);

$event_9 = new Event(
  '5:30p - 6:30p <img class="img-responsive" src="img/bar.jpg">',
'
  <div id="happy-hour">
    <h3>AdClubSTL and Creatives On Call host a complimentary networking happy hour with food in the Fox Sports Midwest Live bar.</h3>
    <p>Sponsored by:</p>
    <img class="img-responsive" src="img/coc.png">
  </div>
  '
);

?>

<div class="section" id="agenda">
  <div class="container">

<!-- heading -->
    <div class="row">
      <div class="col-md-12 section-heading">
        <h2><?php echo $heading; ?></h2>
      </div>
    </div>

<!-- agenda single -->
    <?php foreach (Event::$allEvents as $event): ?>
      <div class="row agenda-single">
        <div class="col-md-4 agenda-left">
          <h3><?php echo ($event->time);?></h3>
        </div>
        <div class="col-md-8 agenda-right">
          <p><?php echo ($event->description);?>
          </p>
        </div>
      </div>
    <?php endforeach; ?>


  </div>
</div>
