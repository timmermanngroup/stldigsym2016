<?php
  include 'inc/0_head.php';
  include 'inc/1_nav.php';
  include 'inc/2_hero.php';
  include 'inc/3_about.php';
  include 'inc/4_speakers.php';
  include 'inc/5_schedule.php';
  include 'inc/6_location.php';
  include 'inc/7_sponsors.php';
  include 'inc/8_sm-feed.php';
  include 'inc/9_footer.php';
?>
