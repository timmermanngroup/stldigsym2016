/* window scroll shift */

function resizeBanner() {
    var bH = $('#navbar-wrap').height();
    $('#hero').css('marginTop', (bH - 20) + 'px');
}

$(document).ready(function() {
    $('a[href^="#"]').on('click', function(e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 90
        }, 600, 'swing', function() {
            window.location.hash = target;
        });
    });

    $(window).on('resize orientationchange', resizeBanner);
    resizeBanner();
});