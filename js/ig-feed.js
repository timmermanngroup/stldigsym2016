/* <![CDATA[ */
(function() {
    var s = document.createElement('script'),
        t = document.getElementsByTagName('script')[0];
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'http://api.flattr.com/js/0.6/load.js?mode=auto';
    t.parentNode.insertBefore(s, t);
})();
/* ]]> */

function createPhotoElement(photo) {
    var innerHtml = $('<img>')
        .addClass('instagram-image')
        .attr('src', photo.images.thumbnail.url);

    innerHtml = $('<a>')
        .attr('target', '_blank')
        .attr('href', photo.link)
        .append(innerHtml);

    return $('<div>')
        .addClass('instagram-placeholder')
        .attr('id', photo.id)
        .append(innerHtml);
}

function didLoadInstagram(event, response) {
    var that = this;

    $.each(response.data, function(i, photo) {
        $(that).append(createPhotoElement(photo));
    });
}

$(document).ready(function() {
    var clientId = 'baee48560b984845974f6b85a07bf7d9';

    $('.instagram.tag').on('didLoadInstagram', didLoadInstagram);
    $('.instagram.tag').instagram({
        hash: 'stldigsym',
        count: 12,
        clientId: clientId
    });
});