<!-- event links -->
<?php $event_link = 'http://www.eventbrite.com/e/stldigsym-st-louis-digital-symposium-2016-tickets-27416631884';
      $facebook = 'https://www.facebook.com/events/747185365415903/';
      $twitter = 'https://twitter.com/adclubstl';
      $linkedin = 'https://www.linkedin.com/grps/Advertising-Club-Saint-Louis-1783457/about';
?>


<!-- speaker class -->
<?php
  class Speaker {
    public $name = null;
    public $title = null;
    public $employer = null;
    public $linkedin = null;
    public $twitter = null;
    public $category = null;
    public $description = null;
    public $content = null;
    public $moderator = null;
    public $panelists = null;
    public static $allSpeakers = array();

    public function __construct($name, $title, $employer, $linkedin, $twitter, $category, $description, $content, $moderator, $panelists) {
      $this->name = $name;
      $this->title = $title;
      $this->employer = $employer;
      $this->linkedin = $linkedin;
      $this->twitter = $twitter;
      $this->category = $category;
      $this->description = $description;
      $this->content = $content;
      $this->moderator = $moderator;
      $this->panelists = $panelists;
      self::$allSpeakers[] = $this;
    }
  }
?>

<!-- event class -->
<?php
  class Event {
    public $time;
    public $description;
    public static $allEvents = array();

    public function __construct($time, $description) {
      $this->time = $time;
      $this->description = $description;
      self::$allEvents[] = $this;
    }
  }
